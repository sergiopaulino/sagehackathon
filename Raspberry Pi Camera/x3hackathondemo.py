#!/usr/bin/env python
# encoding: utf-8
# 04/07/2016
# Raj Senthilnathan
# Acuity Solutions Ltd - Team Acuity
# Written for Sage X3 IOT Hackathon
# import the necessary packages
import ConfigParser
import httplib2
import json
import numpy as np
import cv2
import time
import datetime
import sys
import ConfigParser
import os
import tweepy
from PIL import Image

Config = ConfigParser.ConfigParser()
if os.path.exists("x3hackathondemo.ini"):
    inifile = open("x3hackathondemo.ini", "r+")
    Config.readfp(inifile)
    url = Config.get("settings","x3url")
    productcode1 = Config.get("settings","product1")  
    productcode2 = Config.get("settings","product2")

    #Twitter API credentials
    consumer_key = Config.get("twitter","consumer_key")
    consumer_secret = Config.get("twitter","consumer_secret")
    access_key = Config.get("twitter","access_key")
    access_secret = Config.get("twitter","access_secret")

else:
    print "No config file found!"
    sys.exit()


# send details to Twitter
def tweetdetails(imgenm,message):
        #authorize twitter, initialize tweepy
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_key, access_secret)
        api = tweepy.API(auth)
        
        #tweeting the image
        api.update_with_media(imgenm,status=message)    


# send details to X3 Web Service
def sendStockCountToX3(x3url,item,qty):
    httplib2.debuglevel     = 0
    http                    = httplib2.Http()
    content_type_header     = "application/json"

    data = {    'product':          item,
                'quantity':         qty,
           }

    headers = {'Content-Type': content_type_header}
    print ("Posting %s" % data)
   
    response, content = http.request( x3url,
                                      'POST',
                                      json.dumps(data),
                                      headers=headers)
        
# Get a unique image name based on the current time
def getImageName():
        localtime = time.localtime()
        capturetime = time.strftime("%Y%m%d%H%M%S", localtime)
        return capturetime + ".jpg"
# Main
def main():

        if os.path.exists("x3hackathondemo.ini"):
            inifile = open("x3hackathondemo.ini", "r+")
            Config.readfp(inifile)
            url = Config.get("settings","x3url")
            productcode1 = Config.get("settings","product1")  
            productcode2 = Config.get("settings","product2")         
        else:
            print "No config file found!"
            sys.exit()

        imagename = getImageName()

        vc = cv2.VideoCapture(0)
        cache = ""
        results = None


        if vc.isOpened(): # try to get the first frame
                        rval, frame = vc.read()
        else:
                    rval = False

        while rval:

                    print "Preparing to capture image..."
                    time.sleep(1)
                    rval, frame = vc.read()
                    cache = imagename
                    cv2.imwrite(cache, frame)
                    rval = False

        image = cv2.imread(imagename,1)

        ocrtext = readtextocr(imagename)

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (3, 3), 0)

        # detect edges in the image
        edged = cv2.Canny(gray, 10, 250)

        # construct and apply a closing kernel to 'close' gaps between 'white'
        # pixels
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
        closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)

        # find contours (i.e. the 'outlines') in the image and initialize the
        # total number of products found
        _, cnts, _= cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        total = 0
        product1 = 0
        product2 = 0
        unidentified = 0

        # loop over the contours
        for c in cnts:
        # approximate the contour
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * peri, True)

            if len(approx) == 4:
                # if the approximated contour has four points, then assume that the contour is product 1
                # outline in green
                cv2.drawContours(image, [approx], -1, (0, 255, 0), 4)
                total += 1
                product1 += 1
            elif len(approx) == 3:
                # if the approximated contour has three points, then assume that the contour is product 2
                # outline in blue
                cv2.drawContours(image, [approx], -1, (255, 0, 0), 4)
                total += 1
                product2 += 1
            else:
                # if the approximated contour has a number that is not four or three, then assume that the contour is an unidentified product
                # outline in red
                cv2.drawContours(image, [approx], -1, (0, 0, 255), 4)
                total += 1
                unidentified += 1
           
        # display the output
        message1 = "Found {0} items in that image".format(total)
        message2 = "Found {0} of product 1".format(product1)
        message3 = "Found {0} of product 2".format(product2)
        message4 = "Found {0} unidentified items in that image".format(unidentified)
        message5 = ocrtext

        print message1
        
        print message2
     
        print "connecting to: " + url

        strurl = url + "product=" + productcode1  + "&quantity=" + str(product1) 
        
        sendStockCountToX3(strurl,productcode1,product1)
        
        print message3
         
        strurl = url + "product=" + productcode2 + "&quantity=" + str(product2)

        sendStockCountToX3(strurl,productcode2,product2)
        
        print message4

        strurl = url + "product=" + "&quantity=" + str(unidentified)

        sendStockCountToX3(strurl,"",unidentified)
        
        tweetmessage =  "Raspberry Pi: " + message2 + ". " + message3 + " #SageHackathon #TeamAcuity" + " Text = " + message5

        cv2.imwrite(imagename, image)       

        tweetdetails(imagename,tweetmessage) 

        os.remove(imagename)


if __name__ == '__main__':
    if "-f" in sys.argv:
        main()
    else:
        main()

