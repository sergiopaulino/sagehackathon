<?php
$method = $_SERVER['REQUEST_METHOD'];
$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));

switch ($method) {
  case 'PUT':
    options(); 
    break;
  case 'POST':
    create();
    break;
  case 'GET':
    read();
    break;
  case 'HEAD':
    echo $method; 
    break;
  case 'DELETE':
    delete();
    break;
  case 'OPTIONS':
    options();  
    break;
  default:
    handle_error($request);  
    break;
}

function create(){
	echo 'CREATE';
}

function modify(){
	echo '';
}

function read(){
	echo 'READ';
}

function delete(){
	echo 'DELETE';
}

function options(){
	
	echo '1	GET http://localhost:8080/UserManagement/rest/UserService/users > Get list of users (Read Only)<br>';
	echo '2	GET http://localhost:8080/UserManagement/rest/UserService/users/1 > Get User of Id 1 (Read Only)<br>';
	echo '3	PUT http://localhost:8080/UserManagement/rest/UserService/users/2 > Insert User with Id 2 (Idempotent)<br>';
	echo '4	POST http://localhost:8080/UserManagement/rest/UserService/users/2 > Update User with Id 2 (N/A)<br>';
	echo '5	DELETE http://localhost:8080/UserManagement/rest/UserService/users/1 Delete User with Id 1 (Idempotent)<br>';
	echo '6	OPTIONS http://localhost:8080/UserManagement/rest/UserService/users List the supported operations in web service (Read Only)<br>';
	echo '7	HEAD http://localhost:8080/UserManagement/rest/UserService/users Returns only HTTP Header, no Body. (Read Only)<br>';
}
	

?>
